## UHF-Band Uplink
---
### GMSK TC Flowgraph

![GMSK TC Flowgraph](./media/GMSKModFlowgraph.png "GMSK TC Flowgraph")

This flowgraph modulates a series of repeating 16-bit messages using Precoded GMSK, to test that they can be received and decoded properly by the DSP code on AcubeSAT. The GMSK modulator tested has `Samples per Symbol` set to 10 and `BTs = 0.5`. In fact the I/Q data produced by the flowgraph are identical to those produced by the satellite's GMSK modulator and can be decoded correctly:

![I/Q Comparison](./media/IQComparison.png "I/Q Comparison")

##### Note:
The `GMSK Precoder` block processes a stream of unpacked bits (1 bit of information/byte), whereas the `GMSK Mod` block uses a packed form (8 bits of information/byte). For this reason the input needs to be unpacked bits and the `Pack K Bits` block has to be between the Precoder and the Modulator.

##### .bin to double shell script
After running the flowgraph, to convert the `.bin` output to a `.txt` containing the
I/Q data as doubles run the following commands:

* `chmod +x bin_to_double.sh`
* `./bin_to_double.sh out/out_gmsk.bin out/iq_gmsk.txt`

Note that you may need to change the `File Sink` Block's path in GNURadio to produce the output file.

The shell script compiles and excecutes the `binBreaker/main.cpp`. What this does is
that it inverts the byte order every 4 bytes and creates 32-bit floats for the real and complex part respectively.
